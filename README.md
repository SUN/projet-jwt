SUN Xueting
MICHEL Estelle


					Rapport de projet Sécurité Logiciel


Notre projet était de :
- créer un service qui puisse gérer l'authentification d'un client, la création d'un token JWT et son envoie. 
- créer un service qui s'occupe du site API REST et de l'accès à un utilisateur, s'il utilise le bon token.
- un client qui envoie une requête au premier service avec ses identifiants. Il obtient un token qui lui permet de se connecter au site de l'API REST.

Pour l'implémentation du projet, nous avons décidé d'utiliser le langage Python et javascrypt.

Validation d'un JWT :
  IL faut générer et envoyer le token au client. Le token est chiffré avec SHA-256. 
La conséquence de cette implémentation est que la durée de vie d'un token est limité.
  
Enrôlement des clients : 
   Dans notre projet, le client doit s'authentifier avec son identifiant et son mot de passe. Ici, on ne gère qu'un seul client. S'il y en a plusieurs, il faudra faire une gestion des mots de passe. Il envoie la requête au service qui est chargé de créer un token.					
La conséquence est que le client doit faire 2 requêtes sécurisées : une pour demander un token et l'autre pour accéder à l'API REST. 

La gestion d'une compromission côté serveur : 
   l'accès à l'API REST se fait uniquement avec le bon token.
   
   
   
Chiffrement symétrique - asymétrique : 



Chiffrement symétrique : la transmission de la clef secrète doit être faite par un moyen sûr.  La clef ne doit pas être usurpée. Ce chiffrement n’assure que la confidentialité des données.


Chiffrement asymétrique : permet en plus de la confidentialité des données, de pouvoir authentifier que c'est le bon service qui est à l’origine du message. Le Chiffrement symétrique est plus rapide et plus facile à implémenter que chiffrement asymétrique. 






